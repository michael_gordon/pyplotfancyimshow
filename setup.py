import setuptools

setuptools.setup(
    name='pyplotfancyimshow',
    version='0.1',
    author='Michael Gordon',
    author_email='michael_gordon1998@web.de',
    description='Helpful function to show multiple images using pyplot',
    long_description_content_type="text/markdown",
    url='https://gitlab.com/michael_gordon/pyplotfancyimshow',
    packages=setuptools.find_packages(),
    install_requires=["numpy", "matplotlib"]
)
