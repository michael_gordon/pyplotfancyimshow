import numpy as np
from matplotlib import pyplot as plt
from copy import deepcopy
from typing import Union, List, Tuple


def _normalize(img: np.ndarray) -> np.ndarray:
    minv = img.min()
    maxv = img.max()
    if minv == maxv:
        c = minv if minv != 0 else 1 
        return img.astype(float)/c
    return (img.astype(float) - minv) / (maxv - minv)


def fancy_imshow(
    imgs: Union[np.ndarray, List[np.ndarray]],
    titles: Union[str,List[str]] = [""],
    normalize: bool = True, 
    show: bool = True,
    plotdims: Tuple[int, int] = (1, 1),
    figsize: Tuple[int, int] = None,
    sup_title: str = None,
    axis: bool = False,
    grid: bool = False,
    tikoffsets: List[int] = None,
    *args,
    **kwargs
):
    """
    This function is ment to help visualize many images at once using pyplot.
    If you put in just one image it's the same as calling plt.figure(), plt.imshow(img) and then plt.show()
    But it offers much more when gives multiple images and additional arguments.
    :param imgs: a single image or a list or tuple of images
    :param titles: the title to display in plotting or a list of titles to match to each given image
    :param normalize: if true the images will be min_max normalized before being displayed.
    :param show: if True to call plt.show() at the end of this function
    :param figsize: Size of the pyplot figure.
    :param sup_title: Super Title to display above all displayed images.
    :param grid: if True draw a grid between each pixel
    :param axis: if True draw x and y axis tiks
    :param tikoffsets: List of integers with the same length as imgs. If provided each displayed tik for imgs[i] is shifted by tikoffsets[i]
    :param *args: all the other positional arguments will be given to imshow
    :param **kwargs: all the other keyword arguments will be given to imshow
    """
    if not isinstance(imgs, list):
        imgs = [imgs]
    if not isinstance(titles, list):
        titles = [titles]
    else:
        titles = deepcopy(titles)
    if figsize is None:
        fig, axs = plt.subplots(*plotdims, squeeze=False)
    else:
        fig, axs = plt.subplots(*plotdims, squeeze=False, figsize=figsize)
    if tikoffsets is not None:
        tikoffsets = deepcopy(tikoffsets)

    for i in range(plotdims[0]):
        for j in range(plotdims[1]):
            try:
                title = titles.pop(0)
            except Exception:
                title = None
            try:
                img = imgs.pop(0)
            except Exception:
                img = None

            ax = axs[i][j]
            if img is not None:
                if normalize:
                    img = _normalize(img)
                if not "extent" in kwargs.keys():
                    kwargs["extent"] = (0, img.shape[1], img.shape[0], 0)
                ax.imshow(img, *args, **kwargs)
                if title:
                    ax.set_title(title)
                if not (grid or axis):
                    ax.axis("off")
                if grid:
                    try:
                        off = tikoffsets.pop(0)
                    except Exception:
                        off = 0
                    ax.set_xticks(np.arange(kwargs["extent"][0], kwargs["extent"][1], 1))
                    ax.set_yticks(np.arange(kwargs["extent"][2], kwargs["extent"][3], 1))
                    ax.set_xticklabels(np.round(np.arange(kwargs["extent"][0] + off, kwargs["extent"][1] + off, 1), 2))
                    ax.set_yticklabels(np.round(np.arange(kwargs["extent"][2] + off, kwargs["extent"][3] + off, 1)))
                    ax.grid(which="major",  color = "w", linestyle = "-", linewidth = 2)
            else:
                ax.axis("off")
    if sup_title is not None:
        plt.suptitle(sup_title)
    plt.tight_layout()
    if show:
        plt.show()

if __name__ == "__main__":
    # some demo code
    img = np.zeros((12,9), dtype=np.uint8)
    img[2:4,2:4]=128
    imgs = [img]
    for i in range(2,7):
        t = img.copy()
        t[::i,::i] = 255
        imgs.append(t)
    fancy_imshow(imgs, ["Input"]+[str(i) for i in range(2,7)], plotdims=(2,3), grid=True, cmap="gray")
    imgs = []
    for s in [10, 100, 200]:
        imgs.append(np.random.normal(0,1,(s,s,3)))
    fancy_imshow(imgs, [f"Standard normal noise\n of size {s}x{s}x3" for s in [10, 100, 200]], plotdims=(1,3))